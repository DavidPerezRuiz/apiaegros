<?php
	require_once 'model/Cita.php';

	$body = json_decode(file_get_contents("php://input"), true);

	$accion=null;

	if (isset($body['accion'])) {
		$accion=$body['accion'];
	}

	$id_cita=null;
	$id_medico=null;
	$id_paciente=null;
	$id_resultado=null;
	$dia=null;
	$hora=null;

	$pag=null;
	$numRegistros=null;

	// Necesario para comprobar permisos
	$id_usuario=null;

	// No son un atributo de citas pero se usa para filtrar
	$dia_inicio=null;
	$dia_fin=null;

	$jwt=null;	

	$filters = [];

	if (isset($body['id_cita'])) {
		$id_cita=$body['id_cita'];
		$filters['id_cita']=$id_cita;
	}
	if (isset($body['id_medico'])) {
		$id_medico=$body['id_medico'];
		$filters['id_medico']=$id_medico;
	}
	if (isset($body['id_paciente'])) {
		$id_paciente=$body['id_paciente'];
		$filters['id_paciente']=$id_paciente;
	}
	if (isset($body['id_resultado'])) {
		$id_resultado=$body['id_resultado'];
		$filters['id_resultado']=$id_resultado;
	}
	if (isset($body['dia'])) {
		$dia=$body['dia'];
		$filters['dia']=$dia;
	}
	if (isset($body['hora'])) {
		$hora=$body['hora'];
		$filters['hora']=$hora;
	}
	if (isset($body['dia_inicio'])) {
		$dia_inicio=$body['dia_inicio'];
		$filters['dia_inicio']=$dia_inicio;
	}
	if (isset($body['dia_fin'])) {
		$dia_fin=$body['dia_fin'];
		$filters['dia_fin']=$dia_fin;
	}


	if (isset($body['id_usuario'])) {
		$id_usuario=$body['id_usuario'];
	}

	if (isset($body['pag'])){
		$pag=$body['pag'];
	}
	if (isset($body['numRegistros'])){
		$numRegistros=$body['numRegistros'];
	}

	if (isset($body['jwt'])){
		$jwt=$body['jwt'];
	}

	switch ($accion) {
		
		case 'buscar':

			try {

				$rol=Cita::comprobarPermisos($jwt,$id_usuario);

				echo('{"success":true,"msg":"Cita encontrada","data":'.json_encode(Cita::buscar($filters,$rol))."}");

			} catch (Exception $e) {

				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'filtrar':

			try {

				$rol=Cita::comprobarPermisos($jwt,$id_usuario);

				echo ('{"success":true,"msg":"Citas encontradas","data":'.json_encode(Cita::filtrar($filters,$numRegistros,$pag,$rol))."}");
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'borrar':
			
			try {
				
				Cita::comprobarPermisos($jwt,$id_usuario,$id_usuario);

				$cita = new Cita($id_cita);

				$cita->borrar();
				echo('{"success":true,"msg":"Cita borrada"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			} 
			break;
		case 'insertar':			

			try {
				
				Cita::comprobarPermisos($jwt,$id_usuario,$id_usuario);

				$cita = new Cita(null,$id_medico,$id_paciente,$dia,$hora);

				$cita->insertar();
				echo('{"success":true,"msg":"Cita insertada"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'modificar':
			try {

				Cita::comprobarPermisos($jwt,$id_usuario,$id_usuario);

				$cita = new Cita($id_cita,$id_medico,$id_paciente,$dia,$hora,$id_resultado);

				$cita->modificar();
				echo('{"success":true,"msg":"Cita modificada"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		default:
			http_response_code(404);
			echo '{"msg":"Accion no selecionada","success":false}';
			break;
	}


?>