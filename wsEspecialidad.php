<?php
	require('model/Especialidad.php');

	$body = json_decode(file_get_contents("php://input"), true);

	$accion=null;

	if (isset($body['accion'])) {
		$accion=$body['accion'];
	}

	$id_especialidad=null;
	$titulo=null;
	$descripcion=null;

	$jwt=null;

	$pag=null;
	$numRegistros=null;
	
	if (isset($body['jwt'])){
		$jwt=$body['jwt'];
	}

	if (isset($body['pag'])){
		$pag=$body['pag'];
	}
	if (isset($body['numRegistros'])){
		$numRegistros=$body['numRegistros'];
	}

	if (isset($body['id_especialidad'])) {
		$id_especialidad=$body['id_especialidad'];
		$filters['id_especialidad']=$id_especialidad;
	}
	if (isset($body['titulo'])) {
		$titulo=$body['titulo'];
		$filters['titulo']=$titulo;
	}
	if (isset($body['descripcion'])) {
		$descripcion=$body['descripcion'];
		$filters['descripcion']=$descripcion;
	}


	switch ($accion) {
		
		case 'buscar':
			try {
				echo('{"success":true,"msg":"Especialidad encontrada","data":'.json_encode(Especialidad::buscar($id_especialidad))."}");
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'listar':
			try {
				echo('{"success":true,"msg":"Especialidades listadas","data":'.json_encode(Especialidad::getListaEspecialidades())."}");
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'borrar':
			
			try {
				Especialidad::comprobarPermisos($jwt);

				$especialidad = new Especialidad($id_especialidad);

				$especialidad->borrar();
				echo('{"success":true,"msg":"Especialidad borrada"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			} 
			break;
		case 'insertar':			

			try {
				Especialidad::comprobarPermisos($jwt);

				$especialidad = new Especialidad(null,$titulo,$descripcion);

				$especialidad->insertar();
				echo('{"success":true,"msg":"Especialidad insertada"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'modificar':
			try {

				Especialidad::comprobarPermisos($jwt);

				$especialidad = new Especialidad($id_especialidad,$titulo,$descripcion);

				$especialidad->modificar();
				echo('{"success":true,"msg":"Especialidad modificada"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		default:
			http_response_code(404);
			echo '{"msg":"Accion no selecionada","success":false}';
			break;
	}


?>