<?php
	
	require('model/Paciente.php');

	$body = json_decode(file_get_contents("php://input"), true);

	$accion=null;

	if (isset($body['accion'])) {
		$accion=$body['accion'];
	}

	$id_paciente=null;
	$dni=null;
	$sip=null;
	$nombre=null;
	$apellido_1=null;
	$apellido_2=null;
	$telefono=null;
	$direccion=null;
	$id_usuario=null;
	$correo=null;
	$contrasena=null;

	$pag=null;
	$numRegistros=null;

	$jwt=null;

	if (isset($body['jwt'])) {
		$jwt=$body['jwt'];
	}

	if (isset($body['pag'])){
		$pag=$body['pag'];
	}
	if (isset($body['numRegistros'])){
		$numRegistros=$body['numRegistros'];
	}

	$filters=[];

	if (isset($body['id_paciente'])) {
		$id_paciente=$body['id_paciente'];
		$filters['id_paciente']=$id_paciente;
	}
	if (isset($body['dni'])) {
		$dni=$body['dni'];
		$filters['dni']=$dni;
	}
	if (isset($body['nombre'])) {
		$nombre=$body['nombre'];
		$filters['nombre']=$nombre;
	}
	if (isset($body['apellido_1'])) {
		$apellido_1=$body['apellido_1'];
		$filters['apellido_1']=$apellido_1;
	}
	if (isset($body['apellido_2'])) {
		$apellido_2=$body['apellido_2'];
		$filters['apellido_2']=$apellido_2;
	}
	if (isset($body['sip'])) {
		$sip=$body['sip'];
		$filters['sip']=$sip;
	}
	if (isset($body['telefono'])) {
		$telefono=$body['telefono'];
		$filters['telefono']=$telefono;
	}
	if (isset($body['direccion'])) {
		$direccion=$body['direccion'];
		$filters['direccion']=$direccion;
	}

	if (isset($body['id_usuario'])) {
		$id_usuario=$body['id_usuario'];
		$filters['id_usuario']=$id_usuario;
	}
	if (isset($body['correo'])) {
		$correo=$body['correo'];
		$filters['correo']=$correo;
	}
	if (isset($body['contrasena'])) {
		$contrasena=$body['contrasena'];
		$filters['contrasena']=$contrasena;
	}

	switch ($accion) {
		
		case 'buscar':

			try {

				Paciente::comprobarPermisosAmbos($jwt,$id_usuario);

				echo('{"success":true,"msg":"Paciente encontrado","data":'.json_encode(Paciente::buscar($id_usuario))."}");
			} catch (Exception $e) {
				echo"{'success':false,'msg':'".$e->getMessage()."'}'";
			}
			break;

		case 'filtrar':

			try {

				Paciente::comprobarPermisos($jwt);

				echo ('{"success":true,"msg":"Pacientes encontrados","data":'.json_encode(Medico::filtrar($numRegistros,$pag,$filters)));
			} catch (Exception $e) {
				echo"{'success':false,'msg':'".$e->getMessage()."'}'";
			}			

			break;
		case 'borrar':
			
			$paciente = new Paciente($id_usuario);

			try {

				Paciente::comprobarPermisos($jwt);

				$paciente->borrar($jwt);
				echo('{"success":true,"msg":"Paciente borrado"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			} 

			break;
		case 'modificar':

			try {
				$paciente = new Paciente($id_usuario,$correo,$contrasena,$id_paciente,$dni,$sip,$nombre,$apellido_1,$apellido_2,$direccion,$telefono);
				try {

					Paciente::comprobarPermisosAmbos($jwt,$id_usuario);

					$paciente->modificar();
					echo('{"success":true,"msg":"Paciente modificado"}');
				} catch (Exception $e) {
					echo'{"success":false,"msg":"'.$e->getMessage().'"}';
				}
			} catch (ArgumentCountError $e) {
				echo('{"msg":"Faltan parametros","success":false}');
			}

			break;
		case 'insertar':	
			try {
				$paciente = new Paciente(null,$correo,$contrasena,null,$dni,$sip,$nombre,$apellido_1,$apellido_2,$direccion,$telefono);
				try {
					$paciente->insertar($jwt);
					echo('{"success":true,"msg":"Paciente insertado"}');
				} catch (Exception $e) {
					echo'{"success":false,"msg":"'.$e->getMessage().'"}';
				}
			} catch (ArgumentCountError $e) {
				echo('{"msg":"Faltan parametros","success":false}');
			}

			break;
		default:
			http_response_code(404);
			echo '{"msg":"Accion no selecionada","success":false}';
			break;
	}


?>