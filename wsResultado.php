<?php
	require('model/Resultado.php');

	$body = json_decode(file_get_contents("php://input"), true);

	$accion=null;

	if (isset($body['accion'])) {
		$accion=$body['accion'];
	}

	$id_resultado=null;
	$tratamiento=null;
	$resumen=null;

	$id_cita=null;

	$id_usuario=null;

	$jwt=null;

	if (isset($body['id_resultado'])) {
		$id_resultado=$body['id_resultado'];
		$filters['id_resultado']=$id_resultado;
	}
	if (isset($body['tratamiento'])) {
		$tratamiento=$body['tratamiento'];
		$filters['tratamiento']=$tratamiento;
	}
	if (isset($body['resumen'])) {
		$resumen=$body['resumen'];
		$filters['resumen']=$resumen;
	}

	if (isset($body['id_cita'])) {
		$id_cita=$body['id_cita'];
	}

	if (isset($body['id_usuario'])) {
		$id_usuario=$body['id_usuario'];
	}

	if (isset($body['jwt'])) {
		$jwt=$body['jwt'];
	}

	switch ($accion) {
		
		case 'buscar':

			try {

				Resultado::comprobarPermisosAmbos($jwt,$id_usuario);

				echo('{"success":"true","msg":"Resultado encontrado","data":'.json_encode(Resultado::buscar($id_resultado))."}");

			} catch (Exception $e) {

				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'borrar':
			
			try {
				
				Resultado::comprobarPermisosAmbos($jwt,$id_usuario);

				$resultado = new Resultado($id_resultado,null,null);

				$resultado->borrar();
				echo('{"success":true,"msg":"Resultado borrado"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			} 
			break;
		case 'insertar':			

			try {
				
				$rol = Resultado::comprobarPermisosAmbos($jwt,$id_usuario);

				$resultado = new Resultado(null,$tratamiento,$resumen);

				$resultado->insertar($id_cita,$rol);
				echo('{"success":true,"msg":"Resultado insertado"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'modificar':
			try {

				Resultado::comprobarPermisosAmbos($jwt,$id_usuario);

				$resultado = new Resultado($id_resultado,$tratamiento,$resumen);

				$resultado->modificar();
				echo('{"success":true,"msg":"Resultado modificado"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		default:
			http_response_code(404);
			echo '{"msg":"Accion no selecionada","success":false}';
			break;
	}


?>