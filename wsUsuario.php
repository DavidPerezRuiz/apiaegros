<?php
	
	require_once('model/Usuario.php');

	
	$body = json_decode(file_get_contents("php://input"), true);

	$accion=null;

	if (isset($body['accion'])) {
		$accion=$body['accion'];
	}

	$id_usuario=null;
	$correo=null;
	$contrasena=null;
	$jwt=null;
	$rol=null;

	if (isset($body['id_usuario'])) {
		$id_usuario=$body['id_usuario'];
	}
	if (isset($body['correo'])) {
		$correo=$body['correo'];
	}
	if (isset($body['contrasena'])) {
		$contrasena=$body['contrasena'];
	}
	if (isset($body['jwt'])) {
		$jwt=$body['jwt'];
	}
	if (isset($body['rol'])) {
		$rol=$body['rol'];
	}
	
	switch ($accion) {
		case 'login':
			try {
				$usuario = new Usuario(null,$correo,$contrasena);
				echo ('{"success":true,"data":'.json_encode($usuario->login())."}");
				
			} catch (Exception $e) {

				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
						
			break;
		case 'validar':
			try {
				$sesion_valida = Usuario::comprobarValidezJWT($jwt,$rol);
				if ($sesion_valida) {

					echo ('{"success":true,"msg":"Sesión válida"}');

				} else {

					echo ('{"success":"false","msg":"Sesión no válida, inicie sesión de nuevo"}');
				}
			} catch (Exception $e) {

				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			
			break;
		default:
			http_response_code(404);
			echo ('{"success":false,"msg":"Accion no seleccionada"}');
			break;
	}
	


?>