<?php
	require('model/Horario.php');

	$body = json_decode(file_get_contents("php://input"), true);

	$accion=null;

	if (isset($body['accion'])) {
		$accion=$body['accion'];
	}

	$filters = [];

	$id_horario=null;
	$dias=null;
	$hora_inicio=null;
	$hora_fin=null;
	$id_medico=null;

	$pag=null;
	$numRegistros=null;

	$jwt=null;

	if (isset($body['jwt'])) {
		$jwt=$body['jwt'];
	}

	if (isset($body['pag'])){
		$pag=$body['pag'];
	}
	if (isset($body['numRegistros'])){
		$numRegistros=$body['numRegistros'];
	}

	if (isset($body['id_horario'])) {
		$id_horario=$body['id_horario'];
		$filters['id_horario']=$id_horario;
	}
	if (isset($body['dias'])) {
		$dias=$body['dias'];
		$filters['dias']=$dias;
	}
	if (isset($body['hora_inicio'])) {
		$hora_inicio=$body['hora_inicio'];
		$filters['hora_inicio']=$hora_inicio;
	}
	if (isset($body['hora_fin'])) {
		$hora_fin=$body['hora_fin'];
		$filters['hora_fin']=$hora_fin;
	}
	if (isset($body['id_medico'])) {
		$id_medico=$body['id_medico'];
		$filters['id_medico']=$id_medico;
	}

	switch ($accion) {
		
		case 'buscar':
			try {
				echo('{"success":true,"msg":"Horario encontrado","data":'.json_encode(Horario::buscar($id_medico))."}");
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'filtrar':
			try {
				echo('{"success":true,"msg":"Horarios encontrados","data":'.json_encode(Horario::filtrar($numRegistros,$pag,$filters))."}");
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'borrar':
			
			try {
				
				Horario::comprobarPermisos($jwt);

				$horario = new Horario(null,null,null,null,$id_medico);

				$horario->borrar();
				echo('{"success":true,"msg":"Horario borrado"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			} 
			break;
		case 'insertar':			

			try {
				
				Horario::comprobarPermisos($jwt);

				$horario = new Horario(null,$dias,$hora_inicio,$hora_fin,$id_medico);

				$horario->insertar();
				echo('{"success":true,"msg":"Horario insertado"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		case 'modificar':
			try {

				Horario::comprobarPermisos($jwt);

				$horario = new Horario($id_horario,$dias,$hora_inicio,$hora_fin,$id_medico);

				$horario->modificar();
				echo('{"success":true,"msg":"Horario modificado"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;
		default:
			http_response_code(404);
			echo '{"msg":"Accion no selecionada","success":false}';
			break;
	}


?>