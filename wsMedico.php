<?php
	
	require('model/Medico.php');

	$body = json_decode(file_get_contents("php://input"), true);

	$accion=null;

	if (isset($body['accion'])) {
		$accion=$body['accion'];
	}

	$id_medico=null;
	$dni=null;
	$nombre=null;
	$apellido_1=null;
	$apellido_2=null;
	$telefono=null;
	$direccion=null;
	$id_especialidad=null;
	$id_usuario=null;
	$correo=null;
	$contrasena=null;

	$pag=null;
	$numRegistros=null;

	$jwt=null;

	if (isset($body['jwt'])) {
		$jwt=$body['jwt'];
	}

	if (isset($body['pag'])){
		$pag=$body['pag'];
	}
	if (isset($body['numRegistros'])){
		$numRegistros=$body['numRegistros'];
	}

	$filters=[];

	if (isset($body['id_medico'])) {
		$id_medico=$body['id_medico'];
		$filters['id_medico']=$id_medico;
	}
	if (isset($body['dni'])) {
		$dni=$body['dni'];
		$filters['dni']=$dni;
	}
	if (isset($body['nombre'])) {
		$nombre=$body['nombre'];
		$filters['nombre']=$nombre;
	}
	if (isset($body['apellido_1'])) {
		$apellido_1=$body['apellido_1'];
		$filters['apellido_1']=$apellido_1;
	}
	if (isset($body['apellido_2'])) {
		$apellido_2=$body['apellido_2'];
		$filters['apellido_2']=$apellido_2;
	}
	if (isset($body['id_especialidad'])) {
		$id_especialidad=$body['id_especialidad'];
		$filters['id_especialidad']=$id_especialidad;
	}
	if (isset($body['telefono'])) {
		$telefono=$body['telefono'];
		$filters['telefono']=$telefono;
	}
	if (isset($body['direccion'])) {
		$direccion=$body['direccion'];
		$filters['direccion']=$direccion;
	}

	if (isset($body['id_usuario'])) {
		$id_usuario=$body['id_usuario'];
		$filters['id_usuario']=$id_usuario;
	}
	if (isset($body['correo'])) {
		$correo=$body['correo'];
		$filters['correo']=$correo;
	}
	if (isset($body['contrasena'])) {
		$contrasena=$body['contrasena'];
		$filters['contrasena']=$contrasena;
	}

	switch ($accion) {
		
		case 'buscar':
			try {
				echo('{"success":"true","msg":"Médico encontrado","data":'.json_encode(Medico::buscar($id_usuario))."}");
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
			break;

		case 'filtrar':
			try {
				echo ('{"success":true,"msg":"Médicos encontrados","data":'.json_encode(Medico::filtrar($numRegistros,$pag,$filters))."}");
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			}
				
			break;
		case 'borrar':

			try {
				
				Medico::comprobarPermisos($jwt);
				$medico = new Medico($id_usuario);

				$medico->borrar();
				echo('{"success":true,"msg":"Medico borrado"}');
			} catch (Exception $e) {
				echo'{"success":false,"msg":"'.$e->getMessage().'"}';
			} 

			break;
		case 'modificar':

			try {
				$medico = new Medico($id_usuario,$correo,$contrasena,$id_medico,$dni,$nombre,$apellido_1,$apellido_2,$id_especialidad,$direccion,$telefono);
				try {
					
					Medico::comprobarPermisosAmbos($jwt,$id_medico);

					$medico->modificar();
					echo('{"msg":"Medico modificado","success":true}');
				} catch (Exception $e) {
					echo'{"success":false,"msg":"'.$e->getMessage().'"}';
				}
			} catch (ArgumentCountError $e) {
				echo('{"msg":"Faltan parametros","success":false}');
			}

			break;
		case 'insertar':	
			try {
				$medico = new Medico(null,$correo,$contrasena,null,$dni,$nombre,$apellido_1,$apellido_2,$id_especialidad,$direccion,$telefono);
				try {

					Medico::comprobarPermisos($jwt);

					$medico->insertar($jwt);
					echo('{"msg":"Medico insertado","success":true}');
				} catch (Exception $e) {
					echo'{"success":false,"msg":"'.$e->getMessage().'"}';
				}
			} catch (ArgumentCountError $e) {
				echo('{"msg":"Faltan parametros","success":false}');
			}

			break;
		default:
			http_response_code(404);
			echo '{"msg":"Accion no selecionada","success":false}';
			break;
	}


?>