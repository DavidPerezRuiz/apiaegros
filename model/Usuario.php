<?php
	
	require_once "Singleton.php";
	require_once "Medico.php";
	require_once "Paciente.php";
	require_once "Administrador.php";

	/**
	 * Clase de usuario de la que heredarán el resto de usuarios
	 */
	class Usuario 
	{
		private static $secreto = "RcEwVabqqXm3mAZwScuHwjLgJdUr2YGJVSBcKLGR2dMwS56Dqp7QRdPhNgmgfjD29VSAZGFf9vMTBY4sDJatPgge57kucycaWUNFSygqrYFWfUnSQ7VmSRzanqh2xsTKK9sHqCcUbGmRhTMKc35mWREuvpaTTZ7RCfG8ST2g7wDUpAwXpfExFTVqNkALK3DcvdQ3yL9Yw9mZC9YHjTypXzAxgzywR9DWTTasqd5LwhsW4ssFqms6zgdN2nd5PL5qcAdkXurCSrhQSz9LVFcs8GpDaKcvvq3sKW7Wf9srND56ybXEd8hheLecwjLN9vgSuNxVnY2jHM9Qht3FhJZT46BjCzdRdGwkR4RF8smfqsa5mnkmURpnBDMsxSLnUTd8zkrUTkVNWANfph7gLE6Zw8t6244jA7esZAJvChCmNv6awyPzcT6gd7etLfJ23xLZudEL9bQWx8vbLNYRmYg9cZzTLkLhuMFULM6drXQ5hcRDFX9hVmsLXZwSB2PVtVdF";

		protected $id_usuario;
		protected $correo;
		protected $contrasena;

		private $rol;
		private $tiempoExp;
		
		function __construct($id_usuario=null,$correo=null,$contrasena=null)
		{
			$this->id_usuario=$id_usuario;
			$this->correo=$correo;
			$this->contrasena=$contrasena;
		}

		public function login(){
				
			if ($this->correo == null || $this->contrasena == null ) {

				http_response_code(400);
				throw new Exception("Faltan parametros", 1);
			}
			
			$pdo = Singleton::getInstance();
			
			$params=[];
			$params[':correo']=$this->correo;
			$params[':contrasena']=$this->contrasena;


			$consulta = "select * from usuarios where correo=:correo and contrasena=:contrasena";

			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$informacion_usuario=$stmnt->fetch(PDO::FETCH_ASSOC);

			if (empty($informacion_usuario)) {
				http_response_code(404);
				throw new Exception("Usuario no encontrado", 1);
				
			} else {

				$this->id_usuario=$informacion_usuario['id_usuario'];
				$this->rol=$informacion_usuario['rol'];

				if ($this->rol=="Medico") {
					$datosUsuario = Medico::buscar($this->id_usuario);
					$this->tiempoExp = time()+18000;
				} else if ($this->rol=="Paciente") {
					$datosUsuario = Paciente::buscar($this->id_usuario);
					$this->tiempoExp = time()+3600;
				} else if ($this->rol=="Administrador") {
					$datosUsuario = Administrador::buscar($this->id_usuario);
					$this->tiempoExp = time()+1822222200;
				}
				

				$jwt=$this->generarJWT();

				$datosUsuario['id_usuario']=$informacion_usuario['id_usuario'];
				$datosUsuario['correo']=$informacion_usuario['correo'];
				$datosUsuario['rol']=$informacion_usuario['rol'];
				$datosUsuario['jwt']=$jwt;

				http_response_code(200);
				return $datosUsuario;

				
			}

		}

		public function borrar($jwt) {


			if ($this->id_usuario==null) {

				http_response_code(400);
				throw new Exception("Faltan parametros", 1);
			}
			
			$pdo = Singleton::getInstance();

			$params=[];

			$params[':id']=$this->id_usuario;

			$consulta="delete from usuarios where id_usuario = :id";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$borrado = $stmnt->rowCount();

			if ($borrado) {
				return true;
			} else {

				http_response_code(500);
				throw new Exception("Usuario no encontrado", 1);	
			}
				
		}

		private static function base64url_encode($str) {
		    return rtrim(strtr(base64_encode($str), '+/', '-_'), '=');
		}

		private function generarJWT(){
			
			$header = array('alg' => "HS256",'typ' => 'JWT' );
			$payload = array('id_usuario'=>$this->id_usuario, 'correo' => $this->correo, 'rol' => $this->rol, 'exp' => $this->tiempoExp );

			$header_encoded = Usuario::base64url_encode(json_encode($header));
			$payload_encoded = Usuario::base64url_encode(json_encode($payload));
			$signature = hash_hmac('SHA256', "$header_encoded.$payload_encoded", Usuario::$secreto, true);
			$signature_encoded = Usuario::base64url_encode($signature);

			$jwt = "$header_encoded.$payload_encoded.$signature_encoded";

			return $jwt;
		}

		public static function comprobarValidezJWT($jwt,$rol,$id_usuario=null){

			if ($jwt==null || $rol==null) {

				http_response_code(400);
				throw new Exception("Faltan argumentos", 1);		
			}

			$tokenParts = explode('.', $jwt);

			// Controlamos que el formato de JWT es válido.
			if (sizeof($tokenParts)==3) {
				
				$header = base64_decode($tokenParts[0]);
				$payload = base64_decode($tokenParts[1]);
				$signature_provided = $tokenParts[2];

			} else {
				
				http_response_code(400);
				throw new Exception("JWT no valido", 1);
			}

			$header_decoded = json_decode($header);
			$payload_decoded = json_decode($payload);

			// Comprobamos que el header y el payload de la peticion son válidos
			if( $header_decoded->alg==null || $payload_decoded==null ) {
				
				http_response_code(400);
				throw new Exception("Header del JWT no válido", 1);
			}


			if ( $payload_decoded->id_usuario==null || $payload_decoded->correo==null || $payload_decoded->rol==null || $payload_decoded->exp==null ) {

				http_response_code(400);
				throw new Exception("Payload del JWT no válido", 1);
			} 

			if ($payload_decoded->rol!="Administrador" && $payload_decoded->id_usuario!=$id_usuario) {
				http_response_code(403);
				throw new Exception("Token de autenticacion no pertenece a este usuario", 1);
			}

			if ($payload_decoded->rol!=$rol) {

				http_response_code(403);
				throw new Exception("Usuario no cuenta con los permisos para realizar esta accion", 1);
			}

			$expiracion = json_decode($payload)->exp;
			
			if ($expiracion - time() < 0) {

				http_response_code(401);
				throw new Exception("Sesión expirada, inicie sesión de nuevo", 1);
			} 
			
			$base64_url_header = Usuario::base64url_encode($header);
			$base64_url_payload = Usuario::base64url_encode($payload);
			$signature = hash_hmac('SHA256', "$base64_url_header.$base64_url_payload", Usuario::$secreto, true);

			$base64_url_signature = Usuario::base64url_encode($signature);
			
			if ($signature_provided != $base64_url_signature) {

				http_response_code(401);
				throw new Exception("Sesión no valida", 1);
			} else {
				http_response_code(200);
				return true;
			}

		}
	}

?>