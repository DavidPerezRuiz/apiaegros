<?php
	
	require_once 'Cita.php';
	require_once 'Singleton.php';
	require_once 'Usuario.php';

	class Horario {

		private $id_horario;
		private $dias;
		private $hora_inicio;
		private $hora_fin;
		private $id_medico;

		function __construct($id_horario=null, $dias=null, $hora_inicio=null, $hora_fin=null, $id_medico=null){

			$this->id_horario=$id_horario;
			$this->dias=$dias;
			$this->hora_inicio=$hora_inicio;
			$this->hora_fin=$hora_fin;
			$this->id_medico=$id_medico;

		}

		private static function diasRegex($input) {
		    $regex = '/[1-7]+-[1-7]+/i';
		    return preg_match($regex, $input);
		}

		public static function comprobarPermisos($jwt) {
			try {

				Usuario::comprobarValidezJWT($jwt,'Administrador');
			} catch (Exception $e) {

				throw $e;
			}
		}

		public static function buscar($id_medico) {
			
			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {

				throw $e;
			}

			$params=[];
			$params[':id_medico']=$id_medico;

			$consulta = "select * from horarios where id_medico=:id_medico";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$registro=$stmnt->fetch(PDO::FETCH_ASSOC);

			if (empty($registro)) {
				http_response_code(404);
				throw new Exception("Horario no encontrado", 1);
			} else {
				http_response_code(200);
				return $registro;
			}
			
		}

		public static function filtrar($numRegistros, $pag, $filters){

			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			
			$consultaTotalRegistros="select count(*) as totalRegistros from horarios where true"; 

			$consulta="select * from horarios where true";

			$params=[];

			foreach ($filters as $key => $value) {
				
				$params[':'.$key]=$value.'%';
				$consulta.=" and ".$key." like :".$key;
				$consultaTotalRegistros.=" and ".$key." like :".$key;

			}

			$inicio=($pag-1)*$numRegistros;
			
			if ($numRegistros==null or $pag==null) {
				$consulta.=" order by id_horario asc limit 0,5";
			} else {
				$consulta.=" order by id_horario asc limit ".$inicio.",".$numRegistros;
			}
			
			$stmnt = $pdo->prepare($consultaTotalRegistros);
			$stmnt->execute($params);
			$totalRegistros=$stmnt->fetch();

			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$registros=$stmnt->fetchAll(PDO::FETCH_ASSOC);

			$registros['totalRegistros']=$totalRegistros;	
			http_response_code(200);
			return $registros;
			
		}

		public function insertar() {

			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
			
				throw $e;
			}

			if ($this->dias==null || $this->hora_inicio==null || $this->hora_fin==null || $this->id_medico==null) {
				
				http_response_code(400);
				throw new Exception("Faltan parametros", 1);
			}
			if (!Horario::diasRegex($this->dias)) {

				http_response_code(400);
				throw new Exception("Formato de dias incorrecto. Ej: '1-5' (Lunes a Viernes)", 1);
			}

			try {

				$params=[];
				$params[':dias']=$this->dias;
				$params[':hora_inicio']=$this->hora_inicio;
				$params[':hora_fin']=$this->hora_fin;
				$params[':id_medico']=$this->id_medico;

				$consulta = "insert into horarios (dias,hora_inicio,hora_fin,id_medico) values (:dias,:hora_inicio,:hora_fin,:id_medico)";

				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute($params);

				if ($stmnt->rowCount()==0) {
					throw new Exception("No se ha insertado ningun horario", 1);		
				}

				http_response_code(201);
				return true;

			} catch (Exception $e) {

				http_response_code(400);
				throw new Exception("Error al insertar horario", 1);
				
			}

		}

		public function modificar() {
			
			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
			
				throw $e;
			}
			
			try {

				$params=[];
				$params[':dias']=$this->dias;
				$params[':hora_inicio']=$this->hora_inicio;
				$params[':hora_fin']=$this->hora_fin;
				

				$consulta="update horarios set ";

				foreach ($params as $key => $value) {
					if ($value!=null) {

						$consulta.=substr($key, 1)." = ".$key.", ";
					} else {

						unset($params[$key]);
					}

				}

				$consulta=substr($consulta, 0, strlen($consulta)-2);

				$params[':id_medico']=$this->id_medico;

				$consulta.=" where id_medico=:id_medico";

					$stmnt = $pdo->prepare($consulta);
					$stmnt->execute($params);

					if ($stmnt->rowCount()==0) {
						throw new Exception("No se ha modificado ningun horario", 1);		
					}	

					http_response_code(201);
					return true;

			} catch (Exception $e) {

				http_response_code(400);
				throw new Exception("No se ha modificado el horario", 1);
				
			}

		}

		public function borrar(){
			
			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			$params=[];

			$params[':id_medico']=$this->id_medico;

			$consulta="delete from horarios where id_medico = :id_medico";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$borrado = $stmnt->rowCount();

			if ($borrado) {
				return true;
			} else {

				http_response_code(500);
				throw new Exception("Horario no encontrado", 1);	
			}

		}

		private static function comprobarDiaValido($rango,$diaNumero) {
			
			$dia = date('w', strtotime($diaNumero));

			try {
				$dia_inicio=substr($rango, 0, strpos($rango, '-'));
				$dia_fin=substr($rango, strpos($rango, '-')+1, strlen($rango)-1);
				

				if ($dia>=$dia_inicio && $dia<=$dia_fin) {
					return true;
				} else {
					return false;
				}
			} catch (Exception $e) {
				return false;
			}
			
		}

		private static function comprobarHoraValida($id_cita,$hora_inicio,$hora_fin,$hora) {

			try {
				if (strtotime($hora) >= strtotime($hora_inicio) && strtotime($hora) <= strtotime($hora_fin)) {
  				
  					return true;
				} else {

					return false;
				}
			} catch (Exception $e) {
				return false;
			}
			
		}
		
		public static function comprobarDisponibilidad($id_cita,$dia,$hora,$id_medico){

			try {

				$pdo = Singleton::getInstance();


				$horario = Horario::buscar($id_medico);
				
				if ($horario==null) {
					throw new Exception("El médico seleccionado no tiene un horario asignado", 1);
					
				}

				$dia_valido = Horario::comprobarDiaValido($horario['dias'],$dia);
				$hora_valida = Horario::comprobarHoraValida(null,$horario['hora_inicio'],$horario['hora_fin'],$hora);

				$filtros = [];
				$filtros['hora']=$hora;
				$filtros['dia']=$dia;
				$filtros['id_medico']=$id_medico;

				try {
					$cita = Cita::comprobarExistente($id_medico,$dia,$hora);
					
					// Si se esta modificando se comprueba si es la misma
					if ($id_cita!=null && $cita['id_cita']==$id_cita) {
						$cita=null;
					}
				} catch (Exception $e) {
					$cita=null;
				}

				if ($cita==null && $dia_valido && $hora_valida) {

					return true;
				} else {

					if ($cita!=null) {
						http_response_code(409);
						throw new Exception("Ya existe una cita ese dia y hora", 1);
					} 
					if (!$dia_valido) {
						http_response_code(400);
						throw new Exception("Ese dia está fuera del horario del médico", 1);
					}
					if (!$hora_valida) {
						http_response_code(400);
						throw new Exception("Esa hora está fuera del horario del médico", 1);
					}
				}
			} catch (Exception $e) {
			
				throw $e;
			}



		}


	}


?>