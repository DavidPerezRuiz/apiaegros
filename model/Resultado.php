<?php
	
	require_once 'Singleton.php';
	require_once 'Cita.php';
	require_once 'Usuario.php';

	class Resultado {
		
		private $id_resultado;
		private $tratamiento;
		private $resumen;

		function __construct($id_resultado,$tratamiento,$resumen) {
			
			$this->id_resultado=$id_resultado;
			$this->tratamiento=$tratamiento;
			$this->resumen=$resumen;
		}

		public static function comprobarPermisosAmbos($jwt,$id_usuario) {

			$rol=null;
			
			$jwtExceptionM=null;
			$jwtExceptionA=null;

			try {
				Usuario::comprobarValidezJWT($jwt, "Medico", $id_usuario);
			} catch (Exception $e) {

				$jwtExceptionM=$e;
			}

			try {
				Usuario::comprobarValidezJWT($jwt, "Administrador");
			} catch (Exception $e) {
				
				$jwtExceptionA=$e;	
			}

			if ($jwtExceptionM!=null && $jwtExceptionA!=null) {

				throw $jwtExceptionA;
				
			}

			if ($jwtExceptionM==null) {
				$rol="Medico";
			}
			if ($jwtExceptionA==null) {
				$rol="Administrador";
			}

			return $rol;
		}

		public static function getUltimoResultado() {

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}
			
			$consulta = "select id_resultado from resultados order by id_resultado DESC limit 1";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute();
			$registro=$stmnt->fetch(PDO::FETCH_ASSOC);
			
			if (empty($registro)) {
				http_response_code(404);
				throw new Exception("Resultado no encontrado", 1);
			} else {
				http_response_code(200);
				return $registro['id_resultado'];
			}
		}

		public static function buscar($id_resultado) {

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}
			
			$params=[];
			$params[':id_resultado']=$id_resultado;

			$consulta = "select id_resultado, tratamiento, resumen from resultados where id_resultado=:id_resultado";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$registro=$stmnt->fetch(PDO::FETCH_ASSOC);
			
			if (empty($registro)) {
				http_response_code(404);
				throw new Exception("Resultado no encontrado", 1);
			} else {
				http_response_code(200);
				return $registro;
			}
		}

		public function insertar($id_cita, $rol) {

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			if ($id_cita==null) {
				throw new Exception("Faltan los siguientes datos: id_cita", 1);
			} else {
				
				try {
					$cita = Cita::buscar($id_cita,$rol);
				} catch (Exception $e) {
					throw $e;
				}	

				if($cita['id_resultado']!=null){
					throw new Exception("Esta cita ya tiene resultado", 1);
					
				}
			}

			if ($this->tratamiento==null || $this->tratamiento==null) {

				http_response_code(400);
				throw new Exception("Faltan los siguientes datos: tratamiento", 1);	
			}

			$pdo->beginTransaction();

			try {

				$params=[];
				$params[':tratamiento']=$this->tratamiento;
				$params[':resumen']=$this->resumen;

				$consulta = "insert into resultados (tratamiento,resumen) values (:tratamiento,:resumen)";
				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute($params);

				$id_resultado=Resultado::getUltimoResultado();

				if ($stmnt->rowCount()==0 ) {
					throw new Exception("No se ha insertado el resultado", 1);		
				}

				$cita = new Cita($id_cita,null,null,null,null,$id_resultado);
				$cita->añadirResultado();

				$pdo->commit();

				http_response_code(201);

			} catch (Exception $e) {

				$pdo->rollback();
				http_response_code(400);
				throw new Exception("No se ha insertado el resultado", 1);	
				
			}
		
		}

		public function modificar() {
			
			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			try {
				$params=[];
				
				$params[':tratamiento']=$this->tratamiento;
				$params[':resumen']=$this->resumen;

				$consulta="update resultados set ";

				foreach ($params as $key => $value) {
					if ($value!=null) {

						$consulta.=substr($key, 1)." = ".$key.", ";
					} else {
						unset($params[$key]);
					}

				}

				$consulta=substr($consulta, 0, strlen($consulta)-2);

				$params['id_resultado']=$this->id_resultado;

				$consulta.=" where id_resultado=:id_resultado";


				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute($params);
				
				if ($stmnt->rowCount()==0) {
					http_response_code(400);
					throw new Exception("No se ha modificado el resultado", 1);	
				}

				http_response_code(201);

			} catch (Exception $e) {

				http_response_code(400);
				throw new Exception("No se ha modificado el resultado", 1);	
			}
		}

		public function borrar() {

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}
			
			if ($this->id_resultado==null) {

				http_response_code(400);
				throw new Exception("Faltan parametros", 1);
			}
			
			$params=[];

			$params[':id_resultado']=$this->id_resultado;

			$consulta="delete from resultados where id_resultado = :id_resultado";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$borrado = $stmnt->rowCount();

			if ($borrado) {
				return true;
			} else {

				http_response_code(400);
				throw new Exception("Especialidad no encontrada", 1);	
			}
		}
	}


