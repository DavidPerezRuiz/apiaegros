<?php
	
	require_once 'Usuario.php';

	class Administrador extends Usuario
	{
		private $id_administrador;
		private $nombre;
		private $dni;
		private $telefono;

		function __construct($id_usuario=null,$correo=null,$contrasena=null,$id_administrador=null,$dni=null,$nombre=null,$telefono=null)
		{

			parent::__construct($id_usuario,$correo,$contrasena);

			$this->id_administrador=$id_administrador;
			$this->nombre=$nombre;
			$this->dni=$dni;
			$this->telefono=$telefono;

		}

		public static function buscar($id_usuario){
			
			$pdo = Singleton::getInstance();

			$params=[];
			$params[':id_usuario']=$id_usuario;

			$consulta = "select a.id_administrador as id_administrador,dni,telefono,nombre, u.correo as correo from administradores a join usuarios u on a.id_usuario=u.id_usuario where a.id_usuario=:id_usuario";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$registro=$stmnt->fetch(PDO::FETCH_ASSOC);
			
			if (empty($registro)) {
				http_response_code(404);
				throw new Exception("Administrador no encontrado", 1);
			} else {
				http_response_code(200);
				return $registro;
			}
			

		}
	}

?>