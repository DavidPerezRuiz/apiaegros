<?php
	require_once 'Usuario.php';

	class Paciente extends Usuario
	{
		private $id_paciente;
		private $dni;
		private $sip;
		private $nombre;
		private $apellido_1;
		private $apellido_2;
		private $direccion;
		private $telefono;

		function __construct($id_usuario=null,$correo=null,$contrasena=null,$id_paciente=null,$dni=null,$sip=null,$nombre=null,$apellido_1=null,$apellido_2=null,$direccion=null,$telefono=null)
		{

			parent::__construct($id_usuario,$correo,$contrasena);

			$this->id_paciente=$id_paciente;
			$this->nombre=$nombre;
			$this->apellido_1=$apellido_1;
			$this->apellido_2=$apellido_2;
			$this->dni=$dni;
			$this->sip=$sip;
			$this->direccion=$direccion;
			$this->telefono=$telefono;

		}

		public static function comprobarPermisos($jwt) {
			try {
				
				Usuario::comprobarValidezJWT($jwt, "Administrador");
			} catch (Exception $e) {

				throw $e;
			}
		}

		public static function comprobarPermisosAmbos($jwt,$id_usuario) {
			
			$jwtExceptionP=null;
			$jwtExceptionA=null;

			try {
				Usuario::comprobarValidezJWT($jwt, "Paciente", $id_usuario);
			} catch (Exception $e) {

				$jwtExceptionP=$e;
			}

			try {
				Usuario::comprobarValidezJWT($jwt, "Administrador");
			} catch (Exception $e) {
				
				$jwtExceptionA=$e;	
			}

			if ($jwtExceptionP!=null && $jwtExceptionA!=null) {

				throw $jwtExceptionA;
			}
		}

		public static function buscar($id_usuario,$jwt){

			$pdo = Singleton::getInstance();

			$params=[];
			$params[':id_usuario']=$id_usuario;

			$consulta = "select p.id_paciente,dni,telefono,direccion,nombre,apellido_1,apellido_2, u.correo as correo from pacientes p join usuarios u on p.id_usuario=u.id_usuario where p.id_usuario=:id_usuario";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$registro=$stmnt->fetch(PDO::FETCH_ASSOC);
			
			if (empty($registro)) {
				http_response_code(404);
				throw new Exception("Paciente no encontrado", 1);
			} else {
				http_response_code(200);
				return $registro;
			}
			

		}
		
		public static function filtrar($numRegistros,$pag,$filters,$jwt){

			$pdo = Singleton::getInstance();

			$consultaTotalRegistros="select count(*) as totalRegistros from pacientes p join usuarios u on u.id_usuario = p.id_usuario where true"; 

			$consulta="select id_paciente,dni,sip,telefono,apellido_1,apellido_2,direccion,nombre, p.id_usuario as id_usuario, u.correo as correo from pacientes p join usuarios u on u.id_usuario = p.id_usuario where true";

			$params=[];

			foreach ($filters as $key => $value) {
				
				$params[':'.$key]=$value.'%';
				$consulta.=" and ".$key." like :".$key;
				$consultaTotalRegistros.=" and ".$key." like :".$key;

			}

			$inicio=($pag-1)*$numRegistros;
			
			if ($numRegistros==null or $pag==null) {
				$consulta.=" order by p.id_paciente asc limit 0,5";
			} else {
				$consulta.=" order by p.id_paciente asc limit ".$inicio.",".$numRegistros;
			}
			
			
			$stmnt = $pdo->prepare($consultaTotalRegistros);
			$stmnt->execute($params);
			$totalRegistros=$stmnt->fetch();

			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$registros=$stmnt->fetchAll(PDO::FETCH_ASSOC);

			$registros['totalRegistros']=$totalRegistros;	
			return $registros;
			
		}
		
		public function insertar() {

			// Comprobamos si el paciente que se va a insertar tiene todos los datos obligatorios.
			if ($this->dni==null || $this->nombre==null || $this->apellido_1==null || $this->telefono==null ||	$this->direccion==null || $this->correo==null || $this->contrasena==null || $this->sip==null ) {
				
				http_response_code(400);
				throw new Exception("Faltan parámetros", 1);
			}

			$paciente_insertado=false;
			$usuario_insertado=false;

			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			
			// Ya que vamos a insertar en dos tablas diferentes puede ocurrir que uno de los dos falle al insertar. Para prevenir errores en la base de datos se gestiona con transacciones.

			$pdo->beginTransaction();

			try {

				$paramsU=[];

				$paramsU[':correo']=$this->correo;
				$paramsU[':contrasena']=$this->contrasena;

				// Se incluye el rol de medico en esta peticion
				$consultaUsuario="insert into usuarios(
					correo,contrasena,rol) values(:correo,:contrasena,'Paciente')";

				$stmnt1 = $pdo->prepare($consultaUsuario);
				$stmnt1->execute($paramsU);
				
				$consultaIDUsuario = "select id_usuario from usuarios order by id_usuario desc limit 1";

				$stmnt2 = $pdo->prepare($consultaIDUsuario);
				$stmnt2->execute();
				$id_usuario = $stmnt2->fetch(PDO::FETCH_ASSOC)['id_usuario'];

				$params=[];

				$params[':sip']=$this->sip;
				$params[':nombre']=$this->nombre;
				$params[':apellido_1']=$this->apellido_1;
				$params[':apellido_2']=$this->apellido_2;
				$params[':dni']=$this->dni;
				$params[':telefono']=$this->telefono;
				$params[':direccion']=$this->direccion;
				$params[':id_usuario']=$id_usuario;

				$consultaPaciente="insert into pacientes(dni,sip,nombre,apellido_1,apellido_2,direccion,telefono,id_usuario) values(:dni,:sip,:nombre,:apellido_1,:apellido_2,:direccion,:telefono,:id_usuario)";

				$stmnt3 = $pdo->prepare($consultaPaciente);
				$stmnt3->execute($params);
				$paciente_insertado=true;

				if ($stmnt1->rowCount()==0 && $stmnt3->rowCount()==0) {
					throw new Exception("No se ha insertado el paciente", 1);		
				}

				$pdo->commit();
				http_response_code(201);
				return true;
			

			} catch (Exception $e) {

				$pdo->rollBack();
				http_response_code(400);
				throw new Exception("Paciente no ha podido ser insertado", 1);
			}	

		}

		

		public function modificar(){

			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			$pdo->beginTransaction();

			try {

				$params=[];

				$params[':dni']=$this->dni;
				$params[':nombre']=$this->nombre;
				$params[':apellido_1']=$this->apellido_1;
				$params[':apellido_2']=$this->apellido_2;
				$params[':telefono']=$this->telefono;
				$params[':direccion']=$this->direccion;
				$params[':sip']=$this->sip;
				
				$consultaPaciente="update pacientes set ";

				foreach ($params as $key => $value) {
					if ($value!=null) {

						$consultaPaciente.=substr($key, 1)." = ".$key.", ";
					} else {

						if ($key!=":apellido_2") {
							unset($params[$key]);
						} else {
							$consultaPaciente.=substr($key, 1)." = ".$key.", ";
						}
					}

				}
				$consultaPaciente=substr($consultaPaciente, 0, strlen($consultaPaciente)-2);

				$params[':id_usuario']=$this->id_usuario;

				$consultaPaciente.=" where id_usuario=:id_usuario";
				
				$stmnt1 = $pdo->prepare($consultaPaciente);
				$stmnt1->execute($params);
					
				$paramsU=[];

				$paramsU[':correo']=$this->correo;
				$paramsU[':contrasena']=$this->contrasena;

				$consultaUsuario="update usuarios set ";

				$usuario_modificado=false;
				foreach ($paramsU as $key => $value) {
					if ($value!=null) {
						$paramsVacio=true;
						$consultaUsuario.=substr($key, 1)." = ".$key.", ";
					} else {
						unset($paramsU[$key]);
					}
				}
				$consultaUsuario=substr($consultaUsuario, 0, strlen($consultaUsuario)-2);
				$paramsU[':id_usuario']=$this->id_usuario;
				$consultaUsuario.=" where id_usuario=:id_usuario";

				if ($usuario_modificado) {
					$stmnt2 = $pdo->prepare($consultaUsuario);
					$stmnt2->execute($paramsU);
				}

				if ($stmnt1->rowCount()==0 && $usuario_modificado==false) {
					throw new Exception("No se ha modificado el paciente", 1);		
				}

				$pdo->commit();
				http_response_code(201);
				return true;

			} catch (Exception $e) {

				$pdo->rollBack();
				http_response_code(400);
				throw new Exception("Paciente no ha podido ser modificado", 1);
			}	
			
			

		}
		
	}
	

?>
