<?php
	
	require_once "Singleton.php";
	require_once "Usuario.php";

	class Especialidad {

		private $id_especialidad;
		private $titulo;
		private $descripcion;

		function __construct($id_especialidad=null,$titulo=null,$descripcion=null){

			$this->id_especialidad=$id_especialidad;
			$this->titulo=$titulo;
			$this->descripcion=$descripcion;
		}

		public static function comprobarPermisos($jwt) {
			try {

				Usuario::comprobarValidezJWT($jwt,'Administrador');
			} catch (Exception $e) {

				throw $e;
			}
		}

		public static function buscar($id_especialidad){

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}
			
			$params=[];
			$params[':id_especialidad']=$id_especialidad;

			$consulta = "select titulo, descripcion from especialidades where id_especialidad=:id_especialidad";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$registro=$stmnt->fetch(PDO::FETCH_ASSOC);
			
			if (empty($registro)) {
				http_response_code(404);
				throw new Exception("Especialidad no encontrada", 1);
			} else {
				http_response_code(200);
				return $registro;
			}
			
		}

		public static function getListaEspecialidades(){

			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			try {
				$consultaTotalRegistros = "select count(*) as totalregistros from especialidades";
				$consulta = "select id_especialidad, titulo, descripcion from especialidades";

				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute();

				$stmnt_reg = $pdo->prepare($consultaTotalRegistros);
				$stmnt_reg->execute();
				$totalRegistros=$stmnt_reg->fetch()[0];

				$resultado = $stmnt->fetchAll(PDO::FETCH_ASSOC);
				$resultado['totalRegistros']=$totalRegistros;
				return $resultado;
			} catch (Exception $e) {

				http_response_code(500);
				throw $e;
			}
			

		}
		public function insertar(){

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			try {

				$params=[];
				$params[':titulo']=$this->titulo;
				$params[':descripcion']=$this->descripcion;

				$consulta = "insert into especialidades (titulo,descripcion) values (:titulo,:descripcion)";
				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute($params);

				if ($stmnt->rowCount()==0) {
					throw new Exception("No se ha insertado la especialidad", 1);		
				}

				http_response_code(201);
				return true;

			} catch (Exception $e) {

				http_response_code(400);
				throw new Exception("No se ha insertado la especialidad", 1);	
			}
		

		}

		public function modificar(){
			
			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			try {
				$params=[];
				
				$params[':titulo']=$this->titulo;
				$params[':descripcion']=$this->descripcion;

				$consulta="update especialidades set ";

				foreach ($params as $key => $value) {
					if ($value!=null) {

						$consulta.=substr($key, 1)." = ".$key.", ";
					} else {
						unset($params[$key]);
					}

				}

				$consulta=substr($consulta, 0, strlen($consulta)-2);

				$params['id_especialidad']=$this->id_especialidad;

				$consulta.=" where id_especialidad=:id_especialidad";

				

				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute($params);

				if ($stmnt->rowCount()==0) {
					throw new Exception("No se ha modificado ninguna especialidad", 1);		
				}

				http_response_code(201);
				return true;

			} catch (Exception $e) {

				http_response_code(400);
				throw new Exception("No se ha modificado ninguna especialidad", 1);	
			}
		

		}

		public function borrar() {

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}
			
			if ($this->id_especialidad==null) {

				http_response_code(400);
				throw new Exception("Faltan parametros", 1);
			}
			
			$params=[];

			$params[':id_especialidad']=$this->id_especialidad;

			$consulta="delete from especialidades where id_especialidad = :id_especialidad";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			
			$borrado = $stmnt->rowCount();

			if ($borrado) {
				http_response_code(204);
				return true;
			} else {

				http_response_code(500);
				throw new Exception("Especialidad no encontrada", 1);	
			}
				
		}
	}


?>