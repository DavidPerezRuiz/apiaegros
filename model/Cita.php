<?php
	
	require_once 'Singleton.php';
	require_once 'Horario.php';
	require_once 'Usuario.php';

	Class Cita {

		private $id_cita;
		private $id_medico;
		private $id_paciente;
		private $dia;
		private $hora;
		private $id_resultado;

		function __construct($id_cita=null, $id_medico=null, $id_paciente=null, $dia=null, $hora=null, $id_resultado=null){

			$this->id_cita=$id_cita;
			$this->id_medico=$id_medico;
			$this->id_paciente=$id_paciente;
			$this->dia=$dia;
			$this->hora=$hora;
			$this->id_resultado=$id_resultado;

		}

		public static function comprobarPermisos($jwt,$id_usuario){

			$rol=null;

			$jwtExceptionM=null;
			$jwtExceptionP=null;
			$jwtExceptionA=null;

			try {
				
				Usuario::comprobarValidezJWT($jwt, "Administrador");
			} catch (Exception $e) {

				$jwtExceptionA=$e;
			}

			try {

				Usuario::comprobarValidezJWT($jwt, "Medico", $id_usuario);
			} catch (Exception $e) {

				$jwtExceptionM=$e;
			}

			try {
				
				Usuario::comprobarValidezJWT($jwt, "Paciente", $id_usuario);
			} catch (Exception $e) {

				$jwtExceptionP=$e;
			}

			if ($jwtExceptionP!=null && $jwtExceptionM!=null && $jwtExceptionA!=null) {

				throw $jwtExceptionA;
				
			}

			if ($jwtExceptionP==null) {
				$rol="Paciente";
			}
			if ($jwtExceptionM==null) {
				$rol="Medico";
			}
			if ($jwtExceptionA==null) {
				$rol="Administrador";
			}

			return $rol;
		}

		public static function buscar($id_cita,$rol) {

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}


			if ($id_cita==null) {
				
				http_response_code(400);
				throw new Exception("Faltan argumentos", 1);
				
			}

			$params=[];
			$params[':id_cita']=$id_cita;

			$consulta = "select id_medico, id_paciente, dia, hora, id_resultado from citas where id_cita = :id_cita";

			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$registro=$stmnt->fetch(PDO::FETCH_ASSOC);

			if (empty($registro)) {

				http_response_code(404);
				throw new Exception("Cita no encontrada", 1);
			} else if($rol=="Medico" && isset($filters['id_medico']) && $filters['id_medico']!=$registro['id_medico']){

				http_response_code(403);
				throw new Exception("Esta cita no pertenece al usuario con el que se ha realizado la solicitud", 1);
			} else if($rol=="Paciente" && isset($filters['id_paciente']) && $filters['id_paciente']!=$registro['id_paciente']){

				http_response_code(403);
				throw new Exception("Esta cita no pertenece al usuario con el que se ha realizado la solicitud", 1);
			} else {

				http_response_code(200);
				return $registro;
			}

		}

		public static function comprobarExistente($id_medico,$dia,$hora) {

			try {

				$pdo = Singleton::getInstance();
				$params = [];
				$params[':id_medico']=$id_medico;
				$params[':dia']=$dia;
				$params[':hora']=$hora;

				$consulta = "select id_cita from citas where id_medico=:id_medico and dia=:dia and hora=:hora";

				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute($params);
				$registro=$stmnt->fetch(PDO::FETCH_ASSOC);
				
				if (empty($registro)) {
					throw new Exception("Cita no existe", 1);
					
				}
				return $registro;

			} catch (Exception $e) {
				
				throw $e;
			}
		}

		public static function filtrar($filters,$numRegistros,$pag,$rol) {

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			$consultaTotalRegistros="select count(*) as totalRegistros from citas where true ";

			$consulta = "select id_medico, id_paciente, dia, hora, id_resultado from citas where true ";

			$params=[];

			if ( isset($filters['dia_inicio']) && $filters['dia_inicio']!=null && isset($filters['dia_fin']) && $filters['dia_fin']!=null ) {
				
				$params[':dia_inicio']=$filters['dia_inicio'];	
				$params[':dia_fin']=$filters['dia_fin'];				
				$consulta.=" and dia between :dia_inicio and :dia_fin";
				$consultaTotalRegistros.=" and dia between :dia_inicio and :dia_fin";
			}

			if (isset($filters['id_medico']) && $filters['id_medico']!=null) {
				$params[':id_medico']=$filters['id_medico'];
			}
			if (isset($filters['id_paciente']) && $filters['id_paciente']!=null) {
				$params[':id_paciente']=$filters['id_paciente'];
			}

			if ($rol=="Medico" && isset($filters['id_paciente']) && $filters['id_paciente']!=null) {

				$consulta.=" and id_paciente = :id_paciente where id_medico=:id_medico";
				$consultaTotalRegistros.=" and id_paciente = :id_paciente where id_medico=:id_medico";
			}

			if ($rol=="Paciente" && isset($filters['id_medico']) && $filters['id_medico']!=null) {
				
				$consulta.=" and id_medico = :id_medico where id_paciente=:id_paciente";
				$consultaTotalRegistros.=" and id_medico = :id_medico where id_paciente=:id_paciente";
			}

			if ($rol=="Administrador") {
				
				if (isset($filters['id_medico']) && $filters['id_medico']!=null) {
					
					$consulta.=" and id_medico = :id_medico";
					$consultaTotalRegistros.=" and id_medico = :id_medico";
				}
				if (isset($filters['id_paciente']) && $filters['id_paciente']!=null) {
					
					$consulta.=" and id_paciente = :id_paciente";
					$consultaTotalRegistros.=" and id_paciente = :id_paciente";
				}
			}

			if (isset($filters['hora']) && $filters['hora']!=null) {
				$consulta.=" and hora = :hora";
				$consultaTotalRegistros.=" and hora = :hora";
			}

			$inicio=($pag-1)*$numRegistros;
			
			if ($numRegistros==null or $pag==null) {
				$consulta.=" order by id_cita asc limit 0,5";
			} else {
				$consulta.=" order by id_cita asc limit ".$inicio.",".$numRegistros;
			}

			try {

				$stmnt = $pdo->prepare($consultaTotalRegistros);
				$stmnt->execute($params);
				$totalRegistros=$stmnt->fetch()[0];


				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute($params);
				$registros=$stmnt->fetchAll(PDO::FETCH_ASSOC);

			} catch (Exception $e) {
				
				 http_response_code(400);
				 throw $e; 
			}

			if (empty($registros)) {

				http_response_code(404);
				throw new Exception("No se han encontrado citas bajos esas especificaciones", 1);
			} else {

				http_response_code(200);
				$registros['totalRegistros']=$totalRegistros;	
				return $registros;
			}

		}

		public function insertar() {

			if ($this->id_medico==null || $this->id_paciente==null || $this->dia==null || $this->hora==null) {

				http_response_code(400);
				throw new Exception("Faltan parametros", 1);
				
			}
			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			Horario::comprobarDisponibilidad(null,$this->dia,$this->hora,$this->id_medico);

			$params=[];

			$params['id_medico']=$this->id_medico;
			$params['id_paciente']=$this->id_paciente;
			$params['dia']=$this->dia;
			$params['hora']=$this->hora;

			$consulta = "insert into citas (id_medico,id_paciente,dia,hora) values (:id_medico,:id_paciente,:dia,:hora)";

			try {

				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute($params);
				$paciente_insertado=true;
				http_response_code(201);

			} catch (Exception $e) {

				http_response_code(500);
				throw new Exception("Cita no ha podido ser creada", 1);
			}
		
		}

		public function añadirResultado() {
			
			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			$params = [];

			$params['id_resultado']=$this->id_resultado;
			$params['id_cita']=$this->id_cita;

			$consulta="Update citas set id_resultado=:id_resultado where id_cita=:id_cita";

			try {

				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute($params);	
				http_response_code(201);

			} catch (Exception $e) {

				http_response_code(400);
				throw new Exception("No se ha podido añadir resultado a la cita", 1);
				
			}

		}

		public function modificar(){

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			if ($this->id_cita==null || $this->dia==null || $this->hora ==null || $this->id_medico==null) {
				http_response_code(400);
				throw new Exception("Faltan datos por especificar", 1);
				
			}

			Horario::comprobarDisponibilidad($this->id_cita,$this->dia,$this->hora,$this->id_medico);

			$params=[];

			$params['id_medico']=$this->id_medico;
			$params['id_paciente']=$this->id_paciente;
			$params['dia']=$this->dia;
			$params['hora']=$this->hora;

			$consulta = "update citas set ";

			foreach ($params as $key => $value) {
				if ($value!=null) {

					$consulta.=$key." = :".$key.", ";
				} else {

					unset($params[$key]);
				}

			}

			$consulta=substr($consulta, 0, strlen($consulta)-2);

			$params['id_cita']=$this->id_cita;

			$consulta.=" where id_cita=:id_cita";

			try {

				$stmnt = $pdo->prepare($consulta);
				$stmnt->execute($params);

				if ($stmnt->rowCount()==0) {
					throw new Exception("No se ha modificado ninguna cita", 1);		
				}

				http_response_code(201);
					

			} catch (Exception $e) {

				http_response_code(400);
				throw new Exception("Cita no ha podido ser modificada", 1);
			}

			
		}

		public function borrar(){

			try {
				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			$params=[];

			$params[':id_cita']=$this->id_cita;

			$consulta="delete from citas where id_cita = :id_cita";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$borrado = $stmnt->rowCount();

			if ($borrado) {
				http_response_code(204);
				return true;
			} else {

				http_response_code(500);
				throw new Exception("Cita no encontrada", 1);	
			}

		}




	}