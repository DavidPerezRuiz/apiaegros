<?php
	require_once 'Usuario.php';

	class Medico extends Usuario
	{
		private $id_medico;
		private $nombre;
		private $dni;
		private $apellido_1;
		private $apellido_2;
		private $id_especialidad;
		private $direccion;
		private $telefono;

		function __construct($id_usuario=null,$correo=null,$contrasena=null,$id_medico=null,$dni=null,$nombre=null,$apellido_1=null,$apellido_2=null,$id_especialidad=null,$direccion=null,$telefono=null)
		{

			parent::__construct($id_usuario,$correo,$contrasena);

			$this->id_medico=$id_medico;
			$this->nombre=$nombre;
			$this->apellido_1=$apellido_1;
			$this->apellido_2=$apellido_2;
			$this->dni=$dni;
			$this->id_especialidad=$id_especialidad;
			$this->direccion=$direccion;
			$this->telefono=$telefono;

		}

		public static function comprobarPermisos($jwt){
			try {
				
				Usuario::comprobarValidezJWT($jwt, "Administrador");
			} catch (Exception $e) {

				throw $e;
			}
		}

		public static function comprobarPermisosAmbos($jwt,$id_usuario) {

			$jwtExceptionM=null;
			$jwtExceptionA=null;

			try {
				Usuario::comprobarValidezJWT($jwt, "Medico", $id_usuario);
			} catch (Exception $e) {

				$jwtExceptionM=$e;
			}

			try {
				Usuario::comprobarValidezJWT($jwt, "Administrador");
			} catch (Exception $e) {
				
				$jwtExceptionA=$e;	
			}

			if ($jwtExceptionM!=null && $jwtExceptionA!=null) {

				throw $jwtExceptionA;
				
			}
		}

		public static function buscar($id_usuario){
			
			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}


			$params=[];
			$params[':id_usuario']=$id_usuario;

			$consulta = "select m.id_medico,dni,telefono,apellido_1,apellido_2,direccion,nombre,m.id_especialidad,m.id_usuario,e.titulo as esp_nombre, u.correo as correo from medicos m left join especialidades e on m.id_especialidad=e.id_especialidad join usuarios u on u.id_usuario = m.id_usuario where m.id_usuario=:id_usuario";
			
			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$registro=$stmnt->fetch(PDO::FETCH_ASSOC);
			
			if (empty($registro)) {
				http_response_code(404);
				throw new Exception("Medico no encontrado", 1);
			} else {
				http_response_code(200);
				return $registro;
			}
			

		}
		
		public static function filtrar($numRegistros,$pag,$filters){


			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}


			$consultaTotalRegistros="select count(*) as totalRegistros from medicos m left join especialidades e on m.id_especialidad=e.id_especialidad join usuarios u on u.id_usuario = m.id_usuario where true"; 

			$consulta="select m.id_medico,dni,telefono,apellido_1,apellido_2,direccion,nombre,m.id_especialidad,m.id_usuario,e.titulo as esp_nombre, u.correo as correo from medicos m left join especialidades e on m.id_especialidad=e.id_especialidad join usuarios u on u.id_usuario = m.id_usuario where true";

			$params=[];

			foreach ($filters as $key => $value) {
				
				$params[':'.$key]=$value.'%';
				$consulta.=" and ".$key." like :".$key;
				$consultaTotalRegistros.=" and ".$key." like :".$key;

			}

			$inicio=($pag-1)*$numRegistros;
			
			if ($numRegistros==null or $pag==null) {
				$consulta.=" order by m.id_medico asc limit 0,5";
			} else {
				$consulta.=" order by m.id_medico asc limit ".$inicio.",".$numRegistros;
			}

			$stmnt = $pdo->prepare($consultaTotalRegistros);
			$stmnt->execute($params);
			$totalRegistros=$stmnt->fetch()[0];

			$stmnt = $pdo->prepare($consulta);
			$stmnt->execute($params);
			$registros=$stmnt->fetchAll(PDO::FETCH_ASSOC);

			if (empty($registros)) {
				http_response_code(404);
				throw new Exception("Medicos no encontrados", 1);
			}

			$registros['totalRegistros']=$totalRegistros;	

			http_response_code(200);
			return $registros;
			
		}
		
		public function insertar() {

			// Comprobamos si el médico que se va a insertar tiene todos los datos obligatorios.
			if ($this->dni==null || $this->nombre==null || $this->apellido_1==null || $this->telefono==null ||	$this->direccion==null || $this->correo==null || $this->contrasena==null || $this->id_especialidad==null ) {
				
				http_response_code(400);
				throw new Exception("Faltan parámetros", 1);
			}

			$medico_insertado=false;
			$usuario_insertado=false;

			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			
			// Ya que vamos a insertar en dos tablas diferentes puede ocurrir que uno de los dos falle al insertar. Para prevenir errores en la base de datos se gestiona con transacciones.

			$pdo->beginTransaction();

			try {

				$paramsU=[];

				$paramsU[':correo']=$this->correo;
				$paramsU[':contrasena']=$this->contrasena;

				// Se incluye el rol de medico en esta peticion
				$consultaUsuario="insert into usuarios(
					correo,contrasena,rol) values(:correo,:contrasena,'Medico')";

				$stmnt = $pdo->prepare($consultaUsuario);
				$stmnt->execute($paramsU);
				
				$consultaIDUsuario = "select id_usuario from usuarios order by id_usuario desc limit 1";

				$stmnt = $pdo->prepare($consultaIDUsuario);
				$stmnt->execute();

				if ($stmnt->rowCount()==0) {
					throw new Exception("No se ha insertado el médico", 1);		
				}

				$id_usuario = $stmnt->fetch(PDO::FETCH_ASSOC)['id_usuario'];

				$params=[];

				$params[':especialidad']=$this->id_especialidad;
				$params[':nombre']=$this->nombre;
				$params[':apellido_1']=$this->apellido_1;
				$params[':apellido_2']=$this->apellido_2;
				$params[':dni']=$this->dni;
				$params[':telefono']=$this->telefono;
				$params[':direccion']=$this->direccion;
				$params[':id_usuario']=$id_usuario;

				$consultaMedico="insert into medicos(dni,nombre,apellido_1,apellido_2,id_especialidad,direccion,telefono,id_usuario) values(:dni,:nombre,:apellido_1,:apellido_2,:especialidad,:direccion,:telefono,:id_usuario)";

				$stmnt = $pdo->prepare($consultaMedico);
				$stmnt->execute($params);

				if ($stmnt->rowCount()==0) {
					throw new Exception("No se ha insertado el médico", 1);		
				}

				$pdo->commit();
				http_response_code(201);
				return true;
				

			} catch (Exception $e) {

				$pdo->rollBack();
				http_response_code(400);

				throw new Exception("Médico no ha podido ser insertado", 1);	
			}	

		}

		public function modificar(){

			try {

				$pdo = Singleton::getInstance();
			} catch (Exception $e) {
				throw $e;
			}

			
			$pdo->beginTransaction();

			try {

				$params=[];

				$params[':dni']=$this->dni;
				$params[':nombre']=$this->nombre;
				$params[':apellido_1']=$this->apellido_1;
				$params[':apellido_2']=$this->apellido_2;
				$params[':telefono']=$this->telefono;
				$params[':direccion']=$this->direccion;
				$params[':id_especialidad']=$this->id_especialidad;
				var_dump($this->id_usuario);
				var_dump($this->id_especialidad);
				$consultaMedico="update medicos set ";
			
				foreach ($params as $key => $value) {
					if ($value!=null) {

						$consultaMedico.=substr($key, 1)." = ".$key.", ";
					} else {

						if ($key!=":apellido_2") {
							unset($params[$key]);
						} else {
							$consultaMedico.=substr($key, 1)." = ".$key.", ";
						}
					}

				}
				$consultaMedico=substr($consultaMedico, 0, strlen($consultaMedico)-2);

				$params[':id_usuario']=$this->id_usuario;

				$consultaMedico.=" where id_usuario=:id_usuario";
				var_dump($consultaMedico);
				$stmnt1 = $pdo->prepare($consultaMedico);
				$stmnt1->execute($params);

				$medico_modificado=true;

				$paramsU=[];

				$paramsU[':correo']=$this->correo;
				$paramsU[':contrasena']=$this->contrasena;

				$consultaUsuario="update usuarios set ";

				$usuario_modificado=false;
				foreach ($paramsU as $key => $value) {
					if ($value!=null) {
						$usuario_modificado=true;
						$consultaUsuario.=substr($key, 1)." = ".$key.", ";
					} else {
						unset($paramsU[$key]);
					}
				}

				$consultaUsuario=substr($consultaUsuario, 0, strlen($consultaUsuario)-2);
				$paramsU[':id_usuario']=$this->id_usuario;
				$consultaUsuario.=" where id_usuario=:id_usuario";
				

				if ($usuario_modificado) {
					$stmnt2 = $pdo->prepare($consultaUsuario);
					$stmnt2->execute($paramsU);
				} 

				if ($stmnt1->rowCount()==0 && $usuario_modificado==false) {
					throw new Exception("No se ha modificado el médico", 1);
				}
		
				$pdo->commit();
				http_response_code(201);
				return true;		

			} catch (Exception $e) {

				$pdo->rollBack();
				http_response_code(400);
				throw new Exception("Medico no ha podido ser modificado", 1);
			}	
			
			

		}
		
	}
	

?>
