<?php
class Singleton
{
    // Contenedor Instancia de la clase
    private static $instance = NULL;
   
    // Constructor privado, previene la creación de objetos vía new
    private function __construct() { }

    // Clone no permitido
    private function __clone() { }

    // Método singleton 
    public static function getInstance()
    {
        try {
            if (is_null(self::$instance)) {
                require_once ("inc/conf.inc.php");
                $pdo = new PDO("mysql:host=$host;dbname=$db;charset=utf8;port=$port" , $usuario, $pass);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                
                self::$instance = $pdo;
             }

            return self::$instance;
        } catch (Exception $e) {

            http_response_code(503);
            throw $e;
        }
        
    }
}

?>